'use strict';

const Gatherer = require('lighthouse').Gatherer;

class TimeToGetSchedule extends Gatherer {
    afterPass(options) {
        const driver = options.driver;

        return driver.evaluateAsync('window.serviceResponse')
            .then(serviceResponse => {
                if (!serviceResponse) {

                    throw new Error('Unable to find get schedule metrics in page');
                }
                return serviceResponse;
            });
    }
}

module.exports = TimeToGetSchedule;