'use strict';

const Audit = require('lighthouse').Audit;

const MAX_GET_TIME = 4000;

class LoadGetAudit extends Audit {
    static get meta() {
        return {
            category: 'MyPerformance',
            name: 'getSchedule-audit',
            description: 'Response getSchedule',
            failureDescription: 'getSchedule service slow to initialize',
            helpText: 'Used to measure time from navigationStart to when the schedule service response',

            requiredArtifacts: ['TimeToGetSchedule']
        };
    }

    static audit(artifacts) {
        const loadedTime = artifacts.TimeToGetSchedule;

        const belowThreshold = loadedTime <= MAX_GET_TIME;

        return {
            rawValue: loadedTime,
            score: belowThreshold
        };
    }
}

module.exports = LoadGetAudit;
