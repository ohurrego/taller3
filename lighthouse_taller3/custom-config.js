'use strict';

module.exports = {

    extends: 'lighthouse:default',

    passes: [{
        passName: 'defaultPass',
        gatherers: [
            'card-gatherer',
            'getSchedule-gatherer'
        ]
    }],

    audits: [
        'card-audit',
        'getSchedule-audit'
    ],

    categories: {
        ratp_pwa: {
            name: 'Ratp pwa metrics',
            description: 'Metrics for the ratp timetable site',
            audits: [
                {id: 'card-audit', weight: 1}
            ]
        },
        Performance_get: {
            name: 'Performance get service',
            description: 'Metrics for the response get service',
            audits: [
                {id: 'getSchedule-audit', weight: 1}
            ]
        }
    }
};
